const fs = require("fs");
const http = require("http");
const mkdirp = require("mkdirp");
const path = require("path");
const readline = require("readline");
const util = require("util");

// convert them to Promise
const mkdir    = util.promisify(mkdirp     );
const lstat    = util.promisify(fs.lstat   );
const stat     = util.promisify(fs.stat    );
const readdir  = util.promisify(fs.readdir );
const readFile = util.promisify(fs.readFile);

const _STORE_PATH  = path.resolve(path.join(__dirname, "/stored/" ));
const _DELETE_PATH = path.resolve(path.join(__dirname, "/deleted/"));

const CONSTS = {
  CLEAN_BEFORE       : 24 * 36e5,
  LOAD_ONCE_CNT      : 40 ,
  MAX_OLD_CHECK_CNT  : 400,
  MAX_LOAD_CNT       : 1600,
  MAX_ROUND_LOAD_CNT : 200,
}

const SEL = {
  noPermission  : "div.aux-margin .box.box-tip"     ,

  post          : "div.flow-item div.box"           ,
  postBody      : "div.box-content"                 ,
  postBodyText  : "div.box-content pre"             ,
  postBodyImg   : "div.box-content img"             ,
  postHead      : "div.box-header"                  ,
  postId        : "code.box-id"                     ,
  postQuote     : ".flow-item-quote"                ,
  postWrap      : "div.flow-item-row"               ,

  refresh       : "a.control-btn span.icon-refresh" ,

  reply         : "div.flow-reply"                  ,
  replyBody     : "div.box-content"                 ,
  replyWrap     : "div.flow-reply-row"              ,
  tooFreqBtn    : "div.box.box-tip a"               ,

  // I have no idea how to implement this with css selectors
  // We must not search for .flow-item-row, or pids inside
  // post contents will also be matched.
  xPidHead       : `//div[contains(@class, "box-header") and contains(., "<pid>")]`,
};

/** value format: `[pid, repliesCount, storedTime, status]` */
const _stored = {};

let maxCleaned = -1;

async function cleanLocalOldPosts() {
  logger.log("Cleaning too old posts...");
  const localPidFiles = (await dfsRead(_STORE_PATH)).filter(file => file.endsWith(".txt"));
  for(let i = 0; i < localPidFiles.length; i++) {
    const localPidFile = localPidFiles[i];

    const rawJSON = (await readFile(localPidFile)).toString("utf8");
    const data = JSON.parse(rawJSON);

    if(Date.now() - (data.storedAt || Date.now()) >= CONSTS.CLEAN_BEFORE) {
      // that is 24 hours ago
      const pid = data.pid;
      _stored[pid] = undefined;

      logger.log(`Post ${pid} is too old, hence it will be removed from disk.`);

      fs.unlink(localPidFile, () => {});

      const attachment = localPidFile.slice(0, -4) + ".jpeg";
      if(fs.existsSync(attachment)) {
        fs.unlink(attachment, () => {});
      }

      if(pid > maxCleaned) {
        maxCleaned = pid;
      }
    }
  }
}

async function dfsRead(parent, children) {
  let result = [];

  if (!children) {
    children = await readdir(parent);
  }

  for (let i = 0; i < children.length; i++) {
    const child = path.join(parent, children[i]);

    if ((await lstat(child)).isSymbolicLink())
      continue;
    else if ((await stat(child)).isDirectory())
      result = result.concat((await dfsRead(child, (await readdir(child)))));
    else result.push(child);
  }

  return result;
}

async function downloadImage(url, pid) {
  const _url = url.startsWith("https://") ? url.replace("https", "http") : url;
  const ext = url.split(".").pop() || "jpg";
  const dest = path.join(getSavePath(pid), `${pid}.${ext}`);

  if(fs.existsSync(dest)) {
    // already downloaded.
    return;
  }

  await mkdir(getSavePath(pid));
  
  http.get(_url, {
    headers: {
      "accept": "image/webp,image/apng,image/*,*/*;q=0.8",
      "accept-encoding": "identity",
      "accept-language": "en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7",
      "authority": "pkuhelper.pku.edu.cn",
      "cookie": "pku_ip_flag=yes",
      "referer": "http://pkuhelper.pku.edu.cn/hole/",
      "sec-fetch-mode": "no-cors",
      "sec-fetch-site": "same-origin",
      "user-agent": "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18362",
    },
    timeout: 8 * 1000,
  }, res => {
    const { statusCode } = res;
    if(statusCode !== 200) {
      logger.error("RangeError @ downloadImage(url): got status code of", statusCode, "for", url);
      res.resume();
      return;
    }
    res.pipe(fs.createWriteStream(dest));
  });
}

function getMaxStoredPid() {
  let max = -1;
  for(let key in _stored) {
    if(!_stored.hasOwnProperty(key) || _stored[key] === void 0) continue;
    
    if(_stored[key][0] > max) {
      max = _stored[key][0];
    }
  }
  return Promise.resolve(max);
}

function getMinStoredPid() {
  let min = Number.MAX_SAFE_INTEGER;
  for(let key in _stored) {
    if(!_stored.hasOwnProperty(key) || _stored[key] === void 0) continue;
    
    if(_stored[key][0] < min) {
      min = _stored[key][0];
    }
  }
  return Promise.resolve(min);
}

function getPostReplyCount(pid) {
  return _stored[pid] ? _stored[pid][1] : undefined;
}

function getSavePath(pid) {
  return path.resolve(path.join(_STORE_PATH, `${pid}`.slice(0, 5)));
}

function getUserResponse(question) {
    return new Promise(resolve => {
        let answer = "";
        const itfce = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
            prompt: question,
        });
        itfce.prompt();
        itfce.once("line", line => {
          answer = line;
          itfce.close();
        }).once("close", () => {
          resolve(answer);
        });
    });
};

const logger = {
  log(...args) {
    console.log(`[INFO ${padTime(new Date().toLocaleTimeString())}]`, ...args);
  },
  warn(...args) {
    console.log(`[WARN ${padTime(new Date().toLocaleTimeString())}]`, ...args);
  },
  error(...args) {
    console.error(`[ERRO ${padTime(new Date().toLocaleTimeString())}]`, ...args);
  },
};

function joinSelectors(...sels) {
  return sels.join(" ");
}

function markDeletedPosts(deletedPids) {
  deletedPids.forEach(pid => {
    if(_stored[pid][3] !== "dead")
      _stored[pid][3] = "dead";
    else return;

    logger.log(`Post ${pid} is now marked as a dead post.`);

    const SUB_STORE_PATH = getSavePath(pid);
    const SAVE_PATH      = path.resolve(path.join(SUB_STORE_PATH, `${pid}.txt`));
    const ATTACH_PATH    = path.resolve(path.join(SUB_STORE_PATH, `${pid}.jpeg`));

    if(fs.existsSync(ATTACH_PATH)) {
      const TARGET_ATTACH_PATH = path.resolve(path.join(_DELETE_PATH, `${pid}-deleted.jpg`));
      fs.copyFile(ATTACH_PATH, TARGET_ATTACH_PATH, () => {});
    }

    const TARGET_PATH = path.resolve(path.join(_DELETE_PATH, `${pid}-deleted.txt`));
    fs.copyFile(SAVE_PATH, TARGET_PATH, () => {});
  });
}

function padTime(str) {
  const [timeStr, rest] = str.split(" ");
  const [h, m, s] = timeStr.split(":");
  return `${h.padStart(2, "0")}:${m.padStart(2, "0")}:${s.padStart(2, "0")} ${rest}`;
}

async function readLocalData() {
  const localPidFiles = (await dfsRead(_STORE_PATH)).filter(file => file.endsWith(".txt"));
  for(let i = 0; i < localPidFiles.length; i++) {
    const localPidFile = localPidFiles[i];

    const rawJSON = (await readFile(localPidFile)).toString("utf8");

    const data = JSON.parse(rawJSON);
    _stored[data.pid] = [data.pid, data.replies.length, data.storedAt || Date.now(), "restored"];
  }

  logger.log("Posts restored from local storage:", Object.keys(_stored).length);
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

async function storePostsData(data) {
  const postsData = (data && data.length) ? data : [];

  postsData.forEach(async postData => {
    const SUB_STORE_PATH = getSavePath(postData.pid);
    await mkdir(SUB_STORE_PATH);

    const SAVE_PATH = path.resolve(path.join(SUB_STORE_PATH, `${postData.pid}.txt`));
    
    if(!postData.replies) postData.replies = [];

    if(!Number.isInteger(postData.pid)) {
      logger.error(`TypeError @ storePostsData(data): postsData[...].pid ${postData.pid} not valid`);
    }

    if(postData.pid <= maxCleaned) {
      logger.log(`Post ${postData.pid} shouldn't be stored locally. It is too old.`);
      return;
    }

    // deletedPost.replies.length is 0
    if(postData.replies.length === 0 && getPostReplyCount(postData.pid)) {
      logger.log(`Post ${postData.pid} was deleted, we managed to backup it.`);
      return;
    } else if(postData.replies.length === getPostReplyCount(postData.pid)) {
      // already stored locally, & remain unchanged, so no need to update
      logger.log(`Post ${postData.pid} remain unchanged, no need to store again.`);
      return;
    } else if(getPostReplyCount(postData.pid) !== void 0) {
      logger.log(`Post ${postData.pid} (stored on disk before) successfully updated.`)
    } else {
      logger.log(`Post ${postData.pid} successfully stored on disk.`);
    }

    postData.storedAt = Date.now();

    fs.writeFile(SAVE_PATH, JSON.stringify(postData), () => {});
    _stored[postData.pid] = [postData.pid, postData.replies.length, Date.now()];
  });
}

module.exports = {
  CONSTS,
  SEL,
  cleanLocalOldPosts,
  downloadImage,
  getMaxStoredPid,
  getMinStoredPid,
  getPostReplyCount,
  getUserResponse,
  logger,
  joinSelectors,
  readLocalData,
  markDeletedPosts,
  sleep,
  storePostsData,
};
