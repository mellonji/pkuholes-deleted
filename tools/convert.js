const fs = require("fs");
const path = require("path");

if(!process.argv[2]) {
  console.error("Missing argument: file to convert");
  process.exit(1);
}

const target = path.resolve(path.join(__dirname, "../", process.argv[2]));

if(!fs.existsSync(target)) {
  console.error(`file ${target} does not exist.`);
  process.exit(2);
}

let data = {};

try {
  data = JSON.parse(fs.readFileSync(target, { encoding: "utf8" }).toString("utf8"));
} catch (err) {
  console.error(`Failed to read ${process.argv[2]}, or its content is corrupted.`);
  process.exit(3);
}

console.log(data.postContent);
console.log("");
console.log(new Date(data.stamp).toLocaleString());
console.log("");
console.log(data.replies.join("\n"));
