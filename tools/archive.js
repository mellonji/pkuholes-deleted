const fs = require("fs");
const mkdirp = require("mkdirp");
const rimraf = require("rimraf");
const path = require("path");
const { promisify } = require("util");

const lstat = promisify(fs.lstat), stat = promisify(fs.stat), copyFile = promisify(fs.copyFile);
const readdir = promisify(fs.readdir), readFile = promisify(fs.readFile);

const _STORE_PATH = path.resolve(path.join(__dirname, "../", "stored/"));
const _ARCVE_PATH = path.resolve(path.join(__dirname, "../", "archived/", getFolderName()));

const movePromises = [];

function getFolderName() {
  const d = new Date();
  return `${d.toJSON().split("T")[0]}-${d.getHours()}-${d.getMinutes()}-${d.getSeconds()}`;
}

async function archiveLocaPosts() {
  console.log("Archiving stored local posts...");
  const localPidFiles = (await dfsRead(_STORE_PATH)).filter(file => file.endsWith(".txt"));

  if(localPidFiles.length === 0) {
    console.log("Nothing to archive.");
    process.exit(0);
  }

  mkdirp.sync(_ARCVE_PATH);

  for(let i = 0; i < localPidFiles.length; i++) {
    const localPidFile = localPidFiles[i];

    const rawJSON = (await readFile(localPidFile)).toString("utf8");
    const data = JSON.parse(rawJSON);
    const pid = data.pid;

    console.log(`Archiving post ${pid}...`);

    const _ARCVE_FILE_PATH = path.join(_ARCVE_PATH, `/${pid}`);

    movePromises.push(copyFile(localPidFile, _ARCVE_FILE_PATH + ".txt"))

    const attachment = localPidFile.slice(0, -4) + ".jpeg";
    if(fs.existsSync(attachment)) {
      movePromises.push(copyFile(attachment, _ARCVE_FILE_PATH + ".jpeg"));
    }
  }

  await Promise.all(movePromises);

  rimraf(_STORE_PATH + "/*/*", () => {});
  rimraf(_STORE_PATH + "/*", () => {});
}

async function dfsRead(parent, children) {
  let result = [];

  if (!children) {
    children = await readdir(parent);
  }

  for (let i = 0; i < children.length; i++) {
    const child = path.join(parent, children[i]);

    if ((await lstat(child)).isSymbolicLink())
      continue;
    else if ((await stat(child)).isDirectory())
      result = result.concat((await dfsRead(child, (await readdir(child)))));
    else result.push(child);
  }

  return result;
}

archiveLocaPosts();
