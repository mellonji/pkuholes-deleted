const assert = require("assert");
const fs = require("fs");
const puppeteer = require("puppeteer");
const utils = require("./utils.js");
const { CONSTS, SEL, getMaxStoredPid, getMinStoredPid, getUserResponse, logger, sleep } = utils;

utils.readLocalData();

// 所有跟 page 变量有关的函数都在这个文件里，其余在 utils.js 里

/** @type {puppeteer.Browser} */
let browser = null;
/** @type {puppeteer.Page} */
let page = null;

let round = 0, refreshSoon = false, recoverState = false, refreshPostsCount = 0;

const checkAccessPermission = async () => {
  const result = await page.evaluate(sel => {
    const nodes = [].slice.call(document.querySelectorAll(sel));
    return nodes.filter(node => node.textContent.includes("仅限校内用户")).length > 0;
  }, SEL.noPermission);

  if(result) {
    logger.error("It seems that pkuholes can't be accessed from your ip, please check your Pulse Secure.");
    logger.error("By the way, if you are not a muggle, you should know how to fix this ;-)");
    logger.error("Hint: go to read the source code of webholes!");
    process.exit(2);
  }

  logger.log("checkAccessPermission() believes that your ip can access the pkuholes.");
};

const getAlivePostData = async pid => {
  const baseSelector = utils.joinSelectors(SEL.postId, `a[href="##${pid}"]`);
  const postContent = await page.evaluate((base, sel) => {
    function wrap(obj) {
      if(!obj || !obj.parentNode || !obj.parentNode.parentNode ||
        !obj.parentNode.parentNode.parentNode) {
        return { parentNode: { parentNode: { parentNode: { querySelector() {} }}}}
      }
      return obj;
    }
    // I know this is silly, but come on, it works
    const baseNode = wrap(document.querySelector(base)).parentNode.parentNode.parentNode;
    const node = baseNode.querySelector(sel);
    if(node === void 0) {
      return null;
    }
    return node.textContent;
  }, baseSelector, SEL.postBodyText);

  if(postContent === null) {
    return await recover();
  }

  const rawHead = (await page.evaluate((base, sel) => {
    const baseNode = document.querySelector(base).parentNode.parentNode.parentNode;
    return baseNode.querySelector(sel).textContent;
  }, baseSelector, SEL.postHead)).trim();
  assert.ok(rawHead, "rawHeader should not be empty");

  // [starsCount?, repliesCount?, pid, relativeTime, absoluteTime]
  const sections = rawHead.split(/[\xa0\x0a\x0d]+/);
  assert.ok(sections.length >= 3 && sections.length <= 5, `broken head string: ${rawHead}`);

  let starCount, replies;

  switch(sections.length) {
    case 3:
      starCount = 0;
      replies = [];
      break;
    case 4:
      starCount = Number(sections[0]) || 0;
      replies = [];
      break;
    case 5:
      starCount = Number(sections[0]) || 0;
      replies = await getPostReplies(pid);
      break;
  }

  // MM-DD HH:mm:SS
  const stampInfo = sections.pop();
  assert.ok(/^[\s\d\-\:]+$/.test(stampInfo), `broken stamp string: ${stampInfo}`);

  const datetimeStr = `${new Date().getFullYear()}-${stampInfo} UTC+0800`;
  const stamp = Date.parse(datetimeStr);

  const imageLink = await page.evaluate((base, sel) => {
    function wrap(obj) {
      if(!obj || !obj.parentNode || !obj.parentNode.parentNode ||
        !obj.parentNode.parentNode.parentNode) {
        return { parentNode: { parentNode: { parentNode: { querySelector() {} }}}}
      }
      return obj;
    }
    const baseNode = wrap(document.querySelector(base)).parentNode.parentNode.parentNode;
    const imgNode = baseNode.querySelector(sel);
    return imgNode ? imgNode.src : "";
  }, baseSelector, SEL.postBodyImg);

  if(imageLink) {
    utils.downloadImage(imageLink, pid);
  }

  return {
    pid,
    postContent,
    replies,
    stamp,
    starCount,
    status: "alive",
  };
};

const getDeadPostData = pid => {
  return Promise.resolve({
    pid,
    postContent: "我们发现它时，它已经死了。",
    replies: [],
    stamp: Date.now(),
    starCount: -1,
    status: "dead",
  });
};

const getMaxPostedPid = async () => {
  const selector = utils.joinSelectors(SEL.postWrap, SEL.post, SEL.postHead, SEL.postId);
  const rawResult = await page.evaluate(selector => {
    [].slice.call(document.querySelectorAll(".flow-item-quote")).map(node => node.remove());
    return Array.from(
      document.querySelectorAll(selector)
    ).filter(node => node && node.textContent !== void 0).sort((a, b) => {
      return Number(a.textContent.substr(1)) - Number(b.textContent.substr(1));
    }).pop().textContent.substr(1);
  }, selector);
  const result = Number(rawResult);

  assert.ok(rawResult !== "", "result shouldn't be empty str. This indicates a fatal error.");
  assert.ok(Number.isInteger(result), `result should be an integer, got "${rawResult}"`);

  return result;
};

const getMinPostedPid = async () => {
  const selector = utils.joinSelectors(SEL.postWrap, SEL.post, SEL.postHead, SEL.postId);
  const rawResult = await page.evaluate((selPid, selQuote) => {
    [].slice.call(document.querySelectorAll(selQuote)).map(node => node.remove());
    const resultArr = Array.from(
      document.querySelectorAll(selPid)
    ).filter(node => node && node.textContent !== void 0).sort((a, b) => {
      return Number(a.textContent.substr(1)) - Number(b.textContent.substr(1));
    });
    if(resultArr.length > 0) {
      return resultArr[0].textContent.substr(1);
    } else {
      return false;
    }
  }, selector, SEL.postQuote);

  if(rawResult === false) {
    if(recoverState) {
      gracefullyQuit();
    }
    // handle webholes' unexpected behavior
    recoverState = true;
    await recover();
    return await getMinPostedPid();
  }

  recoverState = false;

  const result = Number(rawResult);
  assert.ok(Number.isInteger(result), `result should be an integer, got "${rawResult}"`);
  return result;
};

const getPostsData = async () => {
  await loadEnoughPosts();
  let maxStoredPid = await getMaxStoredPid(), minStoredPid = await getMinStoredPid();
  let maxPostedPid = await getMaxPostedPid(), minPostedPid = await getMinPostedPid();

  logger.log(`max=${maxPostedPid} min=${minPostedPid} storedMax=${maxStoredPid} storedMin=${minStoredPid}`);

  if(maxStoredPid === -1) {
    // store them all
    for(let pid = minPostedPid; pid <= maxPostedPid; pid++) {
      if(!await postExists(pid)) {
        logger.log(`Post ${pid} was found dead.`);
        utils.storePostsData([await getDeadPostData(pid)]);
      } else {
        utils.storePostsData([await getAlivePostData(pid)]);
      }
    }
  } else {
    // store new posts
    while (maxStoredPid < minPostedPid) {
      const limit = CONSTS.MAX_ROUND_LOAD_CNT;
      if(minPostedPid - maxStoredPid >= limit && round > 0) {
        // something bad must have happened!!!
        logger.error(`? How could this be possible? ${limit}+ new posts in no more than 2 minutes?`);
        logger.error("  There must have been bugs! Will quit...");
        process.exit(1);
      } else if(minPostedPid - maxStoredPid >= 800) {
        const input = await getUserResponse("Would load >= 800 posts. Proceeed? (yes/no)");
        if(!["yes", "y", "ok", "true", "1"].includes(input.toLowerCase())) {
          process.exit(1);
        }
      }

      await loadEnoughPosts();

      minPostedPid = await getMinPostedPid();
    }

    for(let pid = maxStoredPid + 1; pid <= maxPostedPid; pid++) {
      if(!await postExists(pid)) {
        logger.log(`We are sad to announce that ${pid} was found dead.`);
        utils.storePostsData([await getDeadPostData(pid)]);
      } else {
        utils.storePostsData([await getAlivePostData(pid)]);
      }
    }

    // check old posts (no more than 300 posts)
    minStoredPid = await getMinPostedPid();
    minPostedPid = await getMinPostedPid();
    const upperLimit = maxStoredPid - CONSTS.MAX_OLD_CHECK_CNT;
    const deleted = [], startPid = Math.max(Math.min(minStoredPid, minPostedPid), upperLimit);
    logger.log(`Checking older (${startPid} to ${maxStoredPid}) posts...`);
    for(let i = startPid; i < maxStoredPid; i++) {
      if(!await postExists(i)) {
        deleted.push(i);
        const postData = await getDeadPostData(i);
        utils.storePostsData([postData]);
      } else {
        const postData = await getAlivePostData(i);
        utils.storePostsData([postData]);
      }
    }
    utils.markDeletedPosts(deleted);
  }

  if(!refreshSoon) getPostsData();
};

const getPostReplies = async pid => {
  const rawReplies = (await page.evaluate((base, sel) => {
    // I'm really sorry, but this is not my fault
    const xpathNode = document.evaluate(base, document.getRootNode(), null, XPathResult.ANY_UNORDERED_NODE_TYPE, null).singleNodeValue;
    if(xpathNode === null) {
      return null;
    }
    const baseNode = xpathNode.parentNode.parentNode.parentNode;
    return [].slice.call(baseNode.querySelectorAll(sel)).map(node => node.textContent);
  }, SEL.xPidHead.replace("<pid>", pid), utils.joinSelectors(SEL.replyWrap, SEL.replyBody)));

  if(rawReplies === null) {
    if(recoverState === true) {
      gracefullyQuit();
    }
    recoverState = true;
    await recover();
    return getPostReplies(pid);
  }
  recoverState = false;
  return rawReplies;
}

const gracefullyQuit = () => {
  logger.error("Failed to recover from unexpected state.");
  logger.error(new Error("This error is used to show stack traces.").stack);
  return process.exit(1);
};

const hideAndSeek = async () => {
  const userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36 Edge/18.18362";
  await page.setUserAgent(userAgent);

  await page.evaluateOnNewDocument(() => {
    Object.defineProperty(navigator, "webdriver", {
      get: () => undefined,
    });
  });

  await page.evaluateOnNewDocument(() => {
    window.navigator.chrome = undefined;
  });

  await page.evaluateOnNewDocument(() => {
    Object.defineProperty(navigator, "languages", {
      get: () => ["en-US", "en", "zh-CN"],
    });
  });
};

const loadEnoughPosts = async () => {
  let loadedPostsCnt = (await getMaxPostedPid()) - (await getMinPostedPid());
  let begin = loadedPostsCnt;
  do {
    const shouldSlowDown = await page.evaluate((selFreq, selQuote) => {
      scrollBy(0, 900 + ~~(Math.random() * 200));

      [].slice.call(document.querySelectorAll(selQuote)).map(node => node.remove());

      const tooFrequentBtns = [].slice.call(
        document.querySelectorAll(selFreq)
      ).filter(node => node.textContent.includes("重新加载"));

      if(tooFrequentBtns.length > 0) {
        tooFrequentBtns.forEach((btn, i) => setTimeout(btn.click(), (i + 1) * 120));
        return true;
      }
      return false;
    }, SEL.tooFreqBtn, SEL.postQuote);

    if(shouldSlowDown) {
      logger.warn("Oops! We are sending requests too frequently. Let's have a rest...");
      await sleep(30000 + 5000 * Math.random());
    }

    await sleep(10000 + 5000 * Math.random());

    const max = await getMaxPostedPid(), min = await getMinPostedPid();
    loadedPostsCnt = max - min - begin;
    logger.log(`Sessioned posts count is ${max} - ${min} = ${loadedPostsCnt + begin};`);
  } while(loadedPostsCnt < CONSTS.LOAD_ONCE_CNT);  // clean state on page refresh
};

const postExists = async pid => {
  let minPostedPid = await getMinPostedPid();
  if(pid + CONSTS.MAX_LOAD_CNT < minPostedPid) {
    logger.error("RangeError @ postExists(pid): pid too small");
    // just assume it exists
    return true;
  }

  if(pid < minPostedPid + CONSTS.LOAD_ONCE_CNT) {
    logger.log(`Scrolling page for post ${pid}...`);
  }
  while(pid < minPostedPid + CONSTS.LOAD_ONCE_CNT) {
    await loadEnoughPosts();
    minPostedPid = await getMinPostedPid();
    logger.log(`Minimum loaded post pid is ${minPostedPid}.`);
  }

  const selector = utils.joinSelectors(SEL.postWrap, SEL.post, SEL.postHead, SEL.postId);

  const result = await page.evaluate((sel, pid) => {
    const existingIds = [].slice.call(document.querySelectorAll(sel)).map(node => node.textContent);
    if(existingIds.length === 0) {
      return null;
    }
    const stillExisting = existingIds.includes("#" + pid);

    return stillExisting;
  }, selector, pid);

  if(result === null) {
    logger.error("ReferenceError @ postExists(pid): dom.evaluate failed.");
    // for daemon
    process.exit(0);
  }

  if(!result) {
    logger.log(`!!! postExists(pid) thinks that post ${pid} was dead !!!`);
  }

  return result;
}

const recover = async () => {
  logger.warn("Unexpected situation: document.querySelectorAll returns empty array.");
  logger.warn("Feature: we don't try to recover, instead we restart.");
  gracefullyQuit();
};

let refreshPageTimerId = null;

const refreshPage = async () => {
  refreshSoon = true;
  logger.log("Mark: before refresh");

  refreshPageTimerId = setTimeout(gracefullyQuit, 30 * 1000);

  await sleep(5000);
  await page.reload({ waitUntil: ["domcontentloaded"] });
  await sleep(6000);
  logger.log("Mark: after refresh");

  clearTimeout(refreshPageTimerId);
  refreshPageTimerId = null;

  refreshPostsCount = 0;
  refreshSoon = false;
};

let refreshPostsTimerId = null;

const refreshPosts = async () => {
  refreshPostsTimerId = setTimeout(gracefullyQuit, 15 * 1000);

  if(refreshPostsCount > 3) {
    logger.warn("The refresh posts button has been clicked 4 times. Should perform a");
    logger.warn("    full reload to avoid potential exceptions.");
    await sleep(1000);
    return await refreshPage();
  }

  logger.log("Mark: before page.evaluate at refreshPosts");

  await page.evaluate(sel => {
    document.querySelector(sel).click();
  }, SEL.refresh);

  await sleep(1000);

  logger.log("Mark: after page.evaluate at refreshPosts");

  clearTimeout(refreshPostsTimerId);
  refreshPostsTimerId = null;

  refreshPostsCount++;
};

const mainLoop = async () => {
  round++;
  refreshSoon = false;
  await utils.cleanLocalOldPosts();

  logger.log("Refreshing posts...");

  await refreshPosts();
  await sleep(6000);

  getPostsData();

  const nextRoundTime = 1000 * ((round === 0 ? 300 : 240 + round * 10) + Math.random() * 60);
  setTimeout(() => { refreshSoon = true; }, nextRoundTime - 10 * 1000);
  setTimeout(mainLoop, nextRoundTime);
};

(async function main() {
  browser = await puppeteer.launch({
    headless: !process.argv.includes("--debug"),
    defaultViewport: null,
  });
  page = await browser.newPage();

  const testUrl = "https://pkuhelper.pku.edu.cn/hole/";
  await page.goto(testUrl);

  await hideAndSeek();
  await checkAccessPermission();

  setTimeout(mainLoop, 6000);
})();

process.on("beforeExit", async () => {
  if(browser) await browser.close();
}).on("unhandledRejection", async rej => {
  if(browser) await browser.close();
  fs.writeSync(
    process.stderr.fd,
    rej.stack || rej.toString()
  );
}).on("uncaughtException", async err => {
  if(browser) await browser.close();
  fs.writeSync(
    process.stderr.fd,
    err.stack || err.toString()
  );
});
