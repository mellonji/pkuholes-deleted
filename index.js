const cp = require("child_process");
const path = require("path");
const { promisify } = require("util")

const exec = promisify(cp.exec);

const spiderPath = path.resolve(path.join(__dirname, "spider.js"));
const logPath = path.resolve(path.join(__dirname, "logs/", `log-${getFilename()}.txt`))

console.log("Daemon: log file path is", logPath);

function getFilename() {
  const d = new Date();
  const dateStr = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`
  const timeStr = `${d.getHours()}-${d.getMinutes()}-${d.getSeconds()}`;
  return `${dateStr}-${timeStr}`;
}

async function startSpider() {
  return await exec(`node "${spiderPath}" --async-stack-traces >> "${logPath}"`);
}

async function startSpiderDebug() {
  return await exec(`node "${spiderPath}" --debug --async-stack-traces >> "${logPath}"`);
}

;(async function main() {
  if(Number(process.versions.node.split(".")[0]) < 12) {
    console.error("Daemon: ! node version should be greater than 12.0.0");
    process.exit(1);
  }
  let func = startSpider;
  if(process.argv.includes("--debug")) {
    func = startSpiderDebug;
  }

  while(true) {
    try {
      const result = await func();
      if(result.stderr) {
        console.error(result.stderr);
      }
    } catch (err) {
      console.error(err);
      // if(err.toString().includes("Most likely the page has been closed.")) {
      //   process.exit(1);
      // }
    }
    console.log("Daemon: ! spider.js quitted unexpectedly. Restarting...");
  }
})();
