# pkuholes-deleted

删，或者不删，这是个问题。

## Install

1. 使用 `git clone`，或者通过右上角下载按钮，下载 zip 压缩包
2. [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
3. [https://yarnpkg.com/en/docs/install](https://yarnpkg.com/en/docs/install)
4. 命令行，`cd` 到项目目录，执行 `yarn install`，此步需要编译依赖项，耗时较长。如果出现 Download Failed，请参考：[这里](https://jingyan.baidu.com/article/d5c4b52ba71fa0da560dc51d.html)

## Run

```shell
$ node index.js
```

## Tools

```shell
$ ls tools/
archive.js  convert.js
```

### Archive old posts

```shell
$ node tools/archive.js
```

### Convert stored json to human readable text

```shell
$ node tools/convert.js path/to/stored-json.txt
```

## Debug

```shell
$ node index.js --debug
```

## License

Apache-2.0 (see LICENSE.md)
